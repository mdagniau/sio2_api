<?php

/**
 * Configuration pour l'accès à la base de données
 */
class Database
{
    /**
     * Hostname
     * @var string
     */
    private $host = "127.0.0.1";
    /**
     * Nom de la base de données
     * @var string
     */
    private $database_name = "quizeo";
    /**
     * User pour se connecter
     * @var string
     */
    private $username = "root";
    /**
     * Password pour se connecter
     * @var string
     */
    private $password = "root";
    //En prod
    /*private $username = "quizeo";
    private $password = "Gwuy15@5";*/

    /**
     * Connection
     * @var
     */
    public $conn;

    /**
     * Permet de se connecter à la base de données
     * @return PDO|null
     */
    public function getConnection(){
        $this->conn = null;
        try{
            $this->conn = new PDO("mysql:host=" . $this->host .
                ";dbname=" . $this->database_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Database could not be connected: " . $exception->getMessage();
        }
        return $this->conn;
    }

    /**
     * Factorisation d'une méthode permettant de renvoyer un tableau d'objets spécifiques
     * à partir d'un statement
     * @param null $stmt Résultat de la requête
     * @param $conn Connexion à la base de données si besoin d'une sous-requête
     * @param $class Type du tableau d'objets que nous souhaitons renvoyer
     * @return array Tableau d'objets de type $class
     * @throws ApiException Si aucun enregistrement, erreur
     */
    public static function createObjectsArray($stmt=null, $conn, $class){
        $arr = array();
        if ($stmt != null && $stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                array_push($arr, new $class($conn, $row));
            }
        } else {
            throw new ApiException("No record found.");
        }
        return $arr;
    }
}