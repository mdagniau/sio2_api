<?php

ini_set('display_errors', E_ALL);

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../api/Read.php';
include_once '../configuration/ApiException.php';
include_once '../configuration/Database.php';
include_once '../classes/Crud.php';
include_once '../classes/Niveau.php';
include_once '../classes/Theme.php';
include_once '../classes/Proposition.php';
include_once '../classes/Question.php';
include_once '../classes/User.php';

