<?php

/**
 * Exception crée pour l'API
 */
class ApiException extends Exception
{
    /**
     * Si erreur, renvoie un json avec la clef "error" et le message
     * @return false|string
     */
    public function errorMessage() {
        http_response_code(404);
        //Renvoie un json avec comme clef "error"
        //et le msg passé en paramètre
        return json_encode(
            array("error" => $this->getMessage())
        );
    }
}