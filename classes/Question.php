<?php

class Question implements Crud
{
    /**
     * Id
     * @var int
     */
    public $id_question;
    /**
     * Question
     * @var string
     */
    public $name;
    /**
     * @var Proposition Réponse à la question
     */
    public $answer;
    /**
     * Anecdote liée à la question
     * @var string
     */
    public $anecdote;
    /**
     * Tableau des propositions liées à la question
     * @var array<Proposition>
     */
    public $propositions;

    /**
     * Connexion
     * @var
     */
    private $conn;
    /**
     * Nom de la table associée à la classe
     * @var string
     */
    private $db_table = "question";

    /**
     * @param $db Db connection : principe du singleton (design pattern)
     * @param null $row Nécessaire pour créer un objet question à partir d'un enregistrement en base de données
     * @throws ApiException Si aucun enregistrement n'est trouvé
     */
    public function __construct($db, $row = null){
        $this->conn = $db;
        if($row != null){
            $this->id_question = $row['id_question'];
            $this->name = $row['name'];
            // On créé un objet Proposition à partir de id_proposition
            // & de proposition_name
            $this->answer = Proposition::createObject($row['id_proposition'], $row['proposition_name']);
            $this->anecdote = $row['anecdote'];
            //Appel d'une méthode pour récupérer l'ensemble des propositions
            $this->propositions = $this->getAllPropositionsFromQuestionId();
        }
    }

    /**
     * Propositions liées à une question
     * @return array<Proposition>
     * @throws ApiException
     */
    public function getAllPropositionsFromQuestionId(){
        $sqlQuery = "SELECT p.id_proposition, p.name FROM proposition p
             WHERE p.id_proposition IN (SELECT q.propositions_id_proposition 
             FROM question_propositions q
             WHERE q.question_id_question =:id) ";

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(":id", $this->id_question);
        $stmt->execute();
        $arr = Database::createObjectsArray($stmt, $this->conn, Proposition::class);
        return $arr;
    }

    /**
     * Toutes les questions en base de données
     * @return array<Question>
     * @throws ApiException
     */
    public function getAll()
    {
        $sqlQuery = "SELECT q.id_question, q.name, q.id_answer, q.anecdote, 
       p.id_proposition as id_proposition, p.name as proposition_name FROM "
            . $this->db_table .
            " q INNER JOIN proposition p ON q.id_answer = p.id_proposition";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        $arr = Database::createObjectsArray($stmt, $this->conn, static::class);
        return $arr;
    }

    public function get()
    {
        // TODO: Implement get() method.
    }

    /**
     * Retourne l'ensemble des questions nécessaires pour l'application
     * @param $theme Id (int)
     * @param $niveau Id (int)
     * @return array<Question> Questions en fonction d'un thème et d'un niveau
     * @throws ApiException Si aucune question n'est trouvée
     */
    public function getQuestionsByThemeAndNiveau($theme, $niveau){
        $sqlQuery = "SELECT q.id_question, q.name, q.id_answer, q.anecdote, 
       p.id_proposition as id_proposition, p.name as proposition_name FROM "
            . $this->db_table .
            " q INNER JOIN proposition p ON q.id_answer = p.id_proposition"
            ." WHERE q.id_question IN (SELECT questions_id_question FROM quizz_questions "
            ." WHERE quizz_id_quizz = (SELECT id_quizz FROM quizz WHERE id_niveau = ".$niveau." AND id_theme =".$theme." ))";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        $arr = Database::createObjectsArray($stmt, $this->conn, static::class);
        return $arr;
    }
}