<?php

/**
 * CRUD : CREATE, READ, UPDATE, DELETE
 */
interface Crud
{
    /**
     * Méthode générique pour renvoyer tous les éléments en base de données
     * @return array
     */
    public function getAll();

    /**
     * Renvoie un objet spécifique issu de la base de données
     * @return object
     */
    public function get();

   /* public function create();
    public function update();
    public function delete();*/
}