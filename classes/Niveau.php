<?php

class Niveau implements Crud
{
    /**
     * Id
     * @var integer
     */
    public $id_niveau;
    /**
     * Nom du niveau
     * @var string
     */
    public $name;

    /**
     * @var Db
     */
    private $conn;
    /**
     * Nom de la table associée à la classe Niveau
     * @var string
     */
    private $db_table = "niveau";

    /**
     * @param $db Db connection : principe du singleton (design pattern)
     * @param null $row Enregistrement si besoin de créer un objet Niveau
     */
    public function __construct($db, $row = null){
        $this->conn = $db;
        if($row != null){
            $this->id_niveau = $row['id_niveau'];
            $this->name = $row['name'];
        }
    }

    /**
     * Renvoie l'ensemble des niveaux en base de données
     * @return array<Niveau>
     * @throws ApiException
     */
    public function getAll()
    {
        $sqlQuery = "SELECT id_niveau, name FROM " . $this->db_table;
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        //Création du tableau d'objets
        $arr = Database::createObjectsArray($stmt, $this->conn, static::class);
        return $arr;
    }

    public function get()
    {
        // TODO: Implement get() method.
        $sqlQuery = "SELECT id_niveau, name, FROM ". $this->db_table .
            " WHERE id_niveau = ? LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->id_niveau);
        $stmt->execute();
        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        //$this->name = $dataRow['name'];
    }
}