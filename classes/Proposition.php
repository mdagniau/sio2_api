<?php

class Proposition implements Crud
{
    /**
     * Id
     * @var integer
     */
    public $id_proposition;
    /**
     * Nom de la proposition
     * @var string
     */
    public $name;

    /**
     * @var Db|null
     */
    private $conn;
    /**
     * Nom de la table associée à la classe Proposition
     * @var string
     */
    private $db_table = "proposition";

    /**
     * @param null $db Db connection : principe du singleton (design pattern)
     * @param null $row Enregistrement issu de la base de données si besoin de créer un objet Proposition
     */
    public function __construct($db=null, $row = null){
        $this->conn = $db;
        if($row != null){
            $this->id_proposition = $row['id_proposition'];
            $this->name = $row['name'];
        }
    }

    /**
     * Permet de créer un objet Proposition à partir d'un id et d'un nom
     * @param $id integer
     * @param $name string
     * @return Proposition
     */
    public static function createObject($id, $name){
        $prop = new self();
        $prop->id_proposition = $id;
        $prop->name = $name;
        return $prop;
    }

    /**
     * Renvoie l'ensemble des propositions en base de données
     * @return array<Proposition>
     * @throws ApiException
     */
    public function getAll()
    {
        $sqlQuery = "SELECT id_proposition, name FROM " . $this->db_table;
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        //Création du tableau d'objets
        $arr = Database::createObjectsArray($stmt, $this->conn, static::class);
        return $arr;
    }

    public function get()
    {
        // TODO: Implement get() method.
    }
}