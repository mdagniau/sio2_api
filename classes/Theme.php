<?php

class Theme implements Crud
{
    /**
     * Id
     * @var integer
     */
    public $id_theme;
    /**
     * Nom du thème
     * @var string
     */
    public $name;

    /**
     * @var Db
     */
    private $conn;
    /**
     * Nom de la table associée à la classe Theme
     * @var string
     */
    private $db_table = "theme";

    /**
     * @param $db Db connection : principe du singleton (design pattern)
     * @param null $row Enregistrement en base de données si besoin de créer un objet Theme
     */
    public function __construct($db, $row = null){
        $this->conn = $db;
        if($row != null){
            $this->id_theme = $row['id_theme'];
            $this->name = $row['name'];
        }
    }

    /**
     * Renvoie l'ensemble des thèmes en base de données
     * @return array<Theme>
     * @throws ApiException
     */
    public function getAll()
    {
        $sqlQuery = "SELECT id_theme, name FROM " . $this->db_table;
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        //Création du tableau d'objets
        $arr = Database::createObjectsArray($stmt, $this->conn, static::class);
        return $arr;
    }

    public function get()
    {
        // TODO: Implement get() method.
    }
}