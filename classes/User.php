<?php

class User
{
    /**
     * Id
     * @var integer
     */
    public $id;
    /**
     * Login
     * @var string
     */
    public $login;
    /**
     * Civilité
     * @var string
     */
    public $civilite;

    /**
     * Connexion à la base de données
     * @var Db
     */
    private $conn;
    /**
     * Nom de la table associée à la classe User
     * @var string
     */
    private $db_table = "user";

    /**
     * @param $db Db connection : principe du singleton (design pattern)
     * @param null $row Enregistrement en base de données si besoin de créer un utilisateur
     */
    public function __construct($db, $row = null){
        $this->conn = $db;
        if($row != null){
            $this->id = $row['id'];
            $this->login = $row['login'];
            $this->civilite = $row['civilite'];
        }
    }

    /**
     * Tous les utilisateurs en base de données
     * @return array<User>
     * @throws ApiException
     */
    public function getAll()
    {
        $sqlQuery = "SELECT u.id, u.login, u.civilite FROM "
            . $this->db_table .
            " u";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        $arr = Database::createObjectsArray($stmt, $this->conn, static::class);
        return $arr;
    }
}