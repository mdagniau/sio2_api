<?php

/**
 * Point d'entrée de l'API après index.php
 */
class Read {

    /**
     * Point d'entrée de l'API : attend au minimum un argument object dans l'URL
     * correspondant à l'objet que l'on souhaite récupérer en base de données
     * @param $object Type d'objet demandé
     * @param null|integer $theme Id du thème choisi
     * @param null|integer $niveau Id du niveau choisi
     * @return json Mot clef error si erreur, tableau d'éléments sinon
     * @throws ApiException
     */
    public static function getRequestElements($object, $theme, $niveau){
        try {
            if(!isset($object)){
                throw new ApiException("Parameter 'object' not found", 404);
            }
            else {
                $classname = ucfirst(strtolower($object));
                if (class_exists($classname)) {
                    //Connexion bdd
                    $db = new Database();
                    $conn = $db->getConnection();

                    //Instanciation classe
                    $items = new $classname($conn);
                    //On met une sécurité sur le type d'objets recherché
                    //& on vérifie qu'on ait bien un thème et un niveau de donné
                    if($object === 'question' &&
                        isset($theme) && isset($niveau)){
                        //Si oui, on appelle la méthode spécifique
                        $arr = $items->getQuestionsByThemeAndNiveau($theme,
                            $niveau);
                    }
                    else {
                        //Si non, getAll() par défaut
                        $arr = $items->getAll();
                    }
                    //On renvoie sous format json
                    return json_encode($arr);
                } else {
                    //Je déclenche une exception si la classe n'est pas trouvée
                    throw new ApiException("Class not found", 404);
                }
            }
        }
            //Si on chope une ApiException, on l'affiche à l'aide de notre méthode
        catch(ApiException $e){
            return $e->errorMessage();
        }
    }
}